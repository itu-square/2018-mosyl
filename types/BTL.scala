// BTL = Basic TAPL language (from the TAPL book)
//       (not to be confused with Bacon Tomato Lattuce)
// This version implemented after "Software Languages" Ralf Lammel. Springer 2018

object BTL {

  sealed trait Expr
  case object True extends Expr
  case object False extends Expr
  case object Zero extends Expr
  case class Succ (inner: Expr) extends Expr // successor
  case class Pred (inner: Expr) extends Expr // predecessor
  case class IsZero (inner: Expr) extends Expr
  case class If (cond: Expr, Then: Expr, Else: Expr) extends Expr

  // This is similar to Peano's inductive definition of natural numbers.  Note
  // the 'inductive' -> it will be natural to reason about the well-formedness
  // using types.




  sealed trait Ty
  case object NatType extends Ty
  case object BoolType extends Ty

  def typeOf (e: Expr): Option[Ty] = e match {
    case True => Some (BoolType)
    case False => Some (BoolType)
    case Zero => Some (NatType)

    case Succ (e) =>
      for { t <- typeOf (e) if t == NatType } // A shorter version: but looks less like an inference rule
      yield NatType                           // typeOf (e) filter { _ == NatType }

    case Pred (e) =>
      for { t <- typeOf (e) if t == NatType }
      yield NatType

    case IsZero (e) =>
      for { t <- typeOf (e) if t == NatType }
      yield BoolType

    case If (e0, e1, e2) =>
      for {
        t0 <- typeOf (e0) if t0 == BoolType
        t1 <- typeOf (e1)
        t2 <- typeOf (e2) if t2 == t1
      } yield t1
  }

  def wellTyped (e: Expr): Boolean = typeOf (e).isDefined

}
